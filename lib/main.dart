import 'package:flutter/material.dart';
import 'file:///C:/Users/juanc/AndroidStudioProjects/tarea_flutter_app/lib/views/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    var assetsImage = new AssetImage('assets/images/logo.jpg');
    var image = new Image(image: assetsImage, width: 120,);

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Login"),
          backgroundColor: Colors.amber[600],
        ),
        body: Container(
            padding: EdgeInsets.symmetric(horizontal: 32),
            child: LoginView(),
        ),
      ),
    );
  }
}